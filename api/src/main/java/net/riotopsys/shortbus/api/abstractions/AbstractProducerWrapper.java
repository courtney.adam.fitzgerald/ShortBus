package net.riotopsys.shortbus.api.abstractions;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public abstract class AbstractProducerWrapper extends AbstractBaseWrapper {

    public abstract Class getClassProduced();

    public abstract void produceEvent(AbstractSubscriptionWrapper subscriberWrapper);

}
