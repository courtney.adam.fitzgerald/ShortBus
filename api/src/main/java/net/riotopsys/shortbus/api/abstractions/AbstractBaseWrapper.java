package net.riotopsys.shortbus.api.abstractions;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by adam.fitzgerald on 6/25/14.
 */
public abstract class AbstractBaseWrapper {

    protected AtomicBoolean active = new AtomicBoolean(true);

    public abstract void setWrappedInstance( Object instance );

    public abstract Object getWrappedInstance( );

    public void deactivate(){
        active.set(false);
    }

}
