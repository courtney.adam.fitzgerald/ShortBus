package net.riotopsys.shortbus.api.abstractions;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public abstract class AbstractSubscriptionWrapper extends AbstractBaseWrapper {

    public abstract Class getClassSubscribedFor();

    public abstract void processEvent(Object event);

}
