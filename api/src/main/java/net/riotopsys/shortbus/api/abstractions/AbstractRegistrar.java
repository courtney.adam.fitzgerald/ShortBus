package net.riotopsys.shortbus.api.abstractions;

import java.util.Set;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public abstract class AbstractRegistrar {

    public abstract  Set<AbstractSubscriptionWrapper> getSubscriptionWrappers(Object object);
    public abstract  Set<AbstractProducerWrapper> getProducersWrappers(Object object);

    protected void populateWrappers(Object object, Set<? extends AbstractBaseWrapper> wrappers){
        for ( AbstractBaseWrapper wrapper: wrappers){
            wrapper.setWrappedInstance(object);
        }
    }

}
