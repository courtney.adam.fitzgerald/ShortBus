package net.riotopsys.shortbus.api;

/**
 * Created by adam.fitzgerald on 6/20/14.
 */
public interface IBus {

    public void register(Object object);
    public void unregister(Object object);
    public void post(Object event);

}
