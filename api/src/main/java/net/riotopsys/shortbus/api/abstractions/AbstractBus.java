package net.riotopsys.shortbus.api.abstractions;

import net.riotopsys.shortbus.api.IBus;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public abstract class AbstractBus implements IBus {


    private Map<Class, Set<AbstractSubscriptionWrapper>> subscriptionLookup = new ConcurrentHashMap<Class, Set<AbstractSubscriptionWrapper>>();
    private Map<Object, Set<AbstractSubscriptionWrapper>> registeredForSubscription = new ConcurrentHashMap<Object, Set<AbstractSubscriptionWrapper>>();

    private Map<Class, AbstractProducerWrapper> productionLookup = new ConcurrentHashMap<Class, AbstractProducerWrapper>();
    private Map<Object, Set<AbstractProducerWrapper>> registeredForProduction = new ConcurrentHashMap<Object, Set<AbstractProducerWrapper>>();

    private Map<Class, Set<Class>> classAncestryCache = new ConcurrentHashMap<Class, Set<Class>>();

    protected abstract AbstractRegistrar getRegistrar();


    @Override
    public void register(Object object) {

        AbstractRegistrar registrar = getRegistrar();

        registerSubscriptions(object, registrar);
        registerProductions(object, registrar);

    }

    private void registerSubscriptions(Object object, AbstractRegistrar registrar) {
        if ( registeredForSubscription.containsKey(object) ){
            throw new RuntimeException("Object was registered already");
        }

        Set<AbstractSubscriptionWrapper> subscriptionWrappers = registrar.getSubscriptionWrappers(object);
        if ( subscriptionWrappers.isEmpty() ){
            //meh.
            return;
        }
        registeredForSubscription.put(object, subscriptionWrappers);

        for ( AbstractSubscriptionWrapper subscriptionWrapper: subscriptionWrappers){
            Class subscribedTo = subscriptionWrapper.getClassSubscribedFor();

            Set<AbstractSubscriptionWrapper> targetSubscriptions = subscriptionLookup.get( subscribedTo );
            if ( targetSubscriptions == null ){
                targetSubscriptions = Collections.newSetFromMap(new ConcurrentHashMap<AbstractSubscriptionWrapper, Boolean>());
                subscriptionLookup.put(subscribedTo, targetSubscriptions);
            }

            targetSubscriptions.add(subscriptionWrapper);

            if (productionLookup.containsKey( subscribedTo )){
                AbstractProducerWrapper producerWrapper = productionLookup.get(subscribedTo);
                if ( producerWrapper.active.get() ) {
                    producerWrapper.produceEvent(subscriptionWrapper);
                }
            }
        }
    }

    private void registerProductions(Object object, AbstractRegistrar registrar) {
        if ( registeredForProduction.containsKey(object) ){
            throw new RuntimeException("Object was registered already");
        }

        Set<AbstractProducerWrapper> producersWrappers = registrar.getProducersWrappers(object);
        if ( producersWrappers.isEmpty() ){
            //meh.
            return;
        }
        registeredForProduction.put(object, producersWrappers);

        for ( AbstractProducerWrapper producerWrapper: producersWrappers){
            Class produceFor = producerWrapper.getClassProduced();

            if (productionLookup.containsKey(produceFor)){
                throw new RuntimeException("only one producer can be in effect for a type.");
            }

            productionLookup.put(produceFor, producerWrapper);
        }
    }

    @Override
    public void unregister(Object object) {

        if ( !registeredForSubscription.containsKey(object) ){
            throw new IllegalArgumentException(String.format("%s object was not registered?", object.getClass()));
        }

        Set<AbstractSubscriptionWrapper> subscriptionWrappers = registeredForSubscription.remove(object);
        Set<AbstractProducerWrapper> producerWrappers = registeredForProduction.remove(object);
        
        Set<AbstractBaseWrapper> wrappers = new HashSet<AbstractBaseWrapper>();
        if ( subscriptionWrappers != null ) {
            wrappers.addAll(subscriptionWrappers);
        }
        if ( producerWrappers != null ) {
            wrappers.addAll(producerWrappers);
        }

        for( AbstractBaseWrapper baseWrapper : wrappers ){
            baseWrapper.deactivate();
            baseWrapper.setWrappedInstance(null);
        }

        if ( subscriptionWrappers != null ) {
            for (AbstractSubscriptionWrapper subscriptionWrapper : subscriptionWrappers) {
                Class subscribedTo = subscriptionWrapper.getClassSubscribedFor();
                Set<AbstractSubscriptionWrapper> targetSubscriptions = subscriptionLookup.get(subscribedTo);
                targetSubscriptions.remove(subscriptionWrapper);
                if (targetSubscriptions.isEmpty()) {
                    subscriptionLookup.remove(subscribedTo);
                }
            }
        }

        if ( producerWrappers != null ) {
            for (AbstractProducerWrapper producerWrapper : producerWrappers) {
                Class produceFor = producerWrapper.getClassProduced();
                productionLookup.remove(produceFor);
            }
        }
    }

    @Override
    public void post(Object event) {
        Set<AbstractSubscriptionWrapper> targetSubscriptions = getAllSubscriptionsForEventClass(event.getClass());

        for (AbstractSubscriptionWrapper target : targetSubscriptions) {
            if (target.active.get()) {
                target.processEvent(event);
            }
        }
    }

    protected Set<AbstractSubscriptionWrapper> getAllSubscriptionsForEventClass(Class clazz){
        Set<AbstractSubscriptionWrapper> result = new HashSet<AbstractSubscriptionWrapper>();

        for ( Class ancestor: getClassAncestry(clazz) ){
            Set<AbstractSubscriptionWrapper> targetSubscriptions = subscriptionLookup.get(ancestor);
            if ( targetSubscriptions != null ){
                result.addAll(targetSubscriptions);
            }
        }

        return result;
    }

    private Set<Class> getClassAncestry(Class clazz){

        if ( clazz == null ){
            return new HashSet<Class>();
        }

        if ( classAncestryCache.containsKey(clazz)){
            return classAncestryCache.get(clazz);
        }

        Set<Class> result = new HashSet<Class>();

        result.add(clazz);

        result.addAll( getClassAncestry( clazz.getSuperclass()) );

        for ( Class interphase: clazz.getInterfaces()){
            result.addAll( getClassAncestry( interphase ) );
        }

        classAncestryCache.put(clazz, result);

        return result;
    }
}
