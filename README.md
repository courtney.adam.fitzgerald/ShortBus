# ShortBus - Yet another event bus 

An event bus built to use compile time code generation over reflection

ShortBus is an event bus designed to decouple different parts of your application
while still allowing them to communicate efficiently.

Loosely based on the [Otto library][1] provided by Square. Loosely as in it has the same outward interface. The innards are wholly different.   

## Usage
For usage instructions please see [the Otto website][1]. Cuz' they give a better description then I do. 

### Where ShortBus is better then Otto
1. Otto only operates on the top level of a class hierarchy. So you cannot register for events subscribed for in the superclass. ShortBus will respect annotations __anywhere__ in the hierarchy.
2. Due to using code gen, annotated methods will not be shown as uncalled in IDE’s like intellij. This is due to the fact that the generated code really does call these methods.
3. Proguarded apps do not need any special rules to exclude the ShortBus api or any annotated methods from processing. This is also due to the clear call path created by the generated code.

### Where Otto is better

1. ShortBus has the potential to create lots of boilerplate code on Android. This could aggravate [The Dalvik 65K methods limit][2] issue.
2. ShortBus is about as alpha as you can get.

###Some gotchas:
1. The bus class is generated and will be named `net.riotopsys.shortbus.gen.Bus`.
2. Because the bus is generated it won't be available in code completion for Eclipse or Android Studio until the project is built once.
3. The bus will not be generated until one `@Subscribe` or `@Produce` is used.
4. During compilation the error `package net.riotopsys.shortbus.gen does not exist
import net.riotopsys.shortbus.gen.Bus;` may appear. Try a clean and full rebuild.

# Download
### Gradle
```
compile 'net.riotopsys.shortbus:shortbus:0.0.1'
compile 'net.riotopsys.shortbus:shortbus-compiler:0.0.1'
```

###Maven
```
<dependency>
    <groupId>net.riotopsys.shortbus</groupId>
    <artifactId>shortbus</artifactId>
    <version>0.0.1</version>
</dependency>
<dependency>
    <groupId>net.riotopsys.shortbus</groupId>
    <artifactId>shortbus-compiler</artifactId>
    <version>0.0.1</version>
</dependency>
```

## Proguard setup

None, it just works.

## License

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```    
## Change log
| Version | Notes |
|---------|-------|
|v0.0.1 | fix event type handeling |
|v0.0.0 | initial release |

 [1]: http://square.github.com/otto/
 [2]: https://code.google.com/p/android/issues/detail?id=20814
