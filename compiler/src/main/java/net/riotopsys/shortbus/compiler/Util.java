package net.riotopsys.shortbus.compiler;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public class Util {

    public static boolean hasVoidReturn(ExecutableElement element) {
        return element.getReturnType().getKind().equals(TypeKind.VOID);
    }

    public static boolean isPublicEnough(Element element) {
        return !element.getModifiers().contains(Modifier.PRIVATE);
    }

    public static boolean isClassOrInterface(Element element) {
        return element.getKind().isClass() || element.getKind().isInterface();
    }

    public static boolean hasDefaultConstructor(BusProcessor busProcessor, TypeElement type) {
        for (ExecutableElement cons : ElementFilter.constructorsIn(type.getEnclosedElements())) {
            if (cons.getParameters().isEmpty() && isPublicEnough(cons))
                return true;
        }
        return false;
    }

    public static PackageElement getPackageElement(Element element) {

        while ( element.getKind() != ElementKind.PACKAGE ){
            element = element.getEnclosingElement();
        }

        return (PackageElement) element;

    }

    public static TypeElement getTypeElement(Element element) {
        while ( true ){
            element = element.getEnclosingElement();
            if ( element.getKind() == ElementKind.CLASS || element.getKind() == ElementKind.INTERFACE ){
                return (TypeElement) element;
            }
        }
    }

    public static TypeElement getParameterElement(ExecutableElement element, ProcessingEnvironment processingEnv) {
        VariableElement parameterElement = element.getParameters().get(0);
        TypeMirror parameterType = parameterElement.asType();
        Types typeUtils = processingEnv.getTypeUtils();
        return (TypeElement) typeUtils.asElement(parameterType);
    }

    public static TypeElement geReturnElement(ExecutableElement element, ProcessingEnvironment processingEnv) {
        Types typeUtils = processingEnv.getTypeUtils();
        return (TypeElement) typeUtils.asElement(element.getReturnType());
    }
}
