package net.riotopsys.shortbus.compiler;

import com.squareup.javawriter.JavaWriter;

import net.riotopsys.shortbus.api.abstractions.AbstractProducerWrapper;
import net.riotopsys.shortbus.api.abstractions.AbstractSubscriptionWrapper;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public class ProducerWrapperWriter {

    private final static String FILE_NAME_FORMAT = "ProducerWrapper$$%s$$%s";

    private final ProcessingEnvironment processingEnv;

    public ProducerWrapperWriter(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    public void write(Set<ExecutableElement> productionMethods) throws IOException {
        for ( ExecutableElement element: productionMethods ){
            write(element);
        }
    }

    public void write(ExecutableElement element) throws IOException {

        TypeElement classElement = Util.getTypeElement(element);
        PackageElement packageElement = Util.getPackageElement(element);
        TypeElement returnElement = Util.geReturnElement(element, processingEnv);

        String wrapperClassName = buildWrapperName(classElement, element);

        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(packageElement.getQualifiedName().toString() + '.' + wrapperClassName);

        new JavaWriter(jfo.openWriter())
                .emitPackage(packageElement.getQualifiedName().toString())
                .emitImports(
                        AbstractProducerWrapper.class.getCanonicalName(),
                        returnElement.getQualifiedName().toString(),
                        AbstractSubscriptionWrapper.class.getCanonicalName())
                .beginType(wrapperClassName, "class", EnumSet.of(Modifier.PUBLIC), AbstractProducerWrapper.class.getCanonicalName())

                .emitField(classElement.getQualifiedName().toString(), "instance", EnumSet.of(Modifier.PRIVATE))

                .emitAnnotation(Override.class)
                .beginMethod("Class", "getClassProduced", EnumSet.of(Modifier.PUBLIC))
                .emitStatement("return %s.class", returnElement.getSimpleName())
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("void", "setWrappedInstance", EnumSet.of(Modifier.PUBLIC), "Object", "object")
                .emitStatement("instance = (%s)object", classElement.getSimpleName().toString())
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("Object", "getWrappedInstance", EnumSet.of(Modifier.PUBLIC))
                .emitStatement("return instance")
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("void", "produceEvent", EnumSet.of(Modifier.PUBLIC), "AbstractSubscriptionWrapper", "subscriberWrapper")
                .beginControlFlow("if ( active.get() )")
                .emitStatement("%s temp = instance.%s()", returnElement.getSimpleName(), element.getSimpleName())
                .beginControlFlow("if ( temp != null )")
                .emitStatement("subscriberWrapper.processEvent(instance.%s())", element.getSimpleName())
                .endControlFlow()
                .endControlFlow()
                .endMethod()

                .endType()
                .close();

    }

    public static String buildWrapperName(TypeElement classElement, ExecutableElement element) {
        return String.format(FILE_NAME_FORMAT, classElement.getSimpleName(), element.getSimpleName());
    }

}
