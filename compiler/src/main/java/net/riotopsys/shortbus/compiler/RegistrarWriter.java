package net.riotopsys.shortbus.compiler;

import com.squareup.javawriter.JavaWriter;

import net.riotopsys.shortbus.api.abstractions.AbstractProducerWrapper;
import net.riotopsys.shortbus.api.abstractions.AbstractRegistrar;
import net.riotopsys.shortbus.api.abstractions.AbstractSubscriptionWrapper;

import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public class RegistrarWriter {

    public static final String REGISTRAR_NAME = "Registrar";

    private final ProcessingEnvironment processingEnv;

    public RegistrarWriter(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    //TODO: refactor this shit
    public void write(Set<ExecutableElement> subscribeMethods, Set<ExecutableElement> produceMethods) throws IOException {

        SubscriptionWrapperWriter subscriptionWrapperWriter = new SubscriptionWrapperWriter(processingEnv);
        ProducerWrapperWriter producerWrapperWriter = new ProducerWrapperWriter(processingEnv);

        Map<TypeElement, Set<ExecutableElement>> subscriptionClassReference = new HashMap<TypeElement, Set<ExecutableElement>>();
        Map<TypeElement, Set<ExecutableElement>> productionClassReference = new HashMap<TypeElement, Set<ExecutableElement>>();

        Set<String> importsNeeded = new HashSet<String>();
        importsNeeded.add(AbstractRegistrar.class.getCanonicalName());
        importsNeeded.add(Set.class.getCanonicalName());
        importsNeeded.add(HashSet.class.getCanonicalName());
        importsNeeded.add(AbstractSubscriptionWrapper.class.getCanonicalName());
        importsNeeded.add(AbstractProducerWrapper.class.getCanonicalName());

        for ( ExecutableElement subscription: subscribeMethods ){
            TypeElement parentClass = Util.getTypeElement(subscription);

            Set<ExecutableElement> classMethods = subscriptionClassReference.get(parentClass);
            if ( classMethods == null ){
                classMethods = new HashSet<ExecutableElement>();
                subscriptionClassReference.put(parentClass, classMethods);
            }

            classMethods.add(subscription);

        }

        removeOverloadedAnnotatedMethods(subscriptionClassReference);

        for ( Entry<TypeElement, Set<ExecutableElement>> entry : subscriptionClassReference.entrySet()){
            PackageElement packageElement = Util.getPackageElement(entry.getKey());
            importsNeeded.add(entry.getKey().getQualifiedName().toString());
            for ( ExecutableElement method: entry.getValue()) {
                importsNeeded.add(packageElement.getQualifiedName().toString() + '.' + SubscriptionWrapperWriter.buildWrapperName(entry.getKey(), method));
                subscriptionWrapperWriter.write(method);
            }
        }

        for ( ExecutableElement producer: produceMethods ){
            TypeElement parentClass = Util.getTypeElement(producer);

            Set<ExecutableElement> classMethods = productionClassReference.get(parentClass);
            if ( classMethods == null ){
                classMethods = new HashSet<ExecutableElement>();
                productionClassReference.put(parentClass, classMethods);
            }

            classMethods.add(producer);

        }

        removeOverloadedAnnotatedMethods(productionClassReference);

        for ( Entry<TypeElement, Set<ExecutableElement>> entry : productionClassReference.entrySet()){
            PackageElement packageElement = Util.getPackageElement(entry.getKey());
            importsNeeded.add(entry.getKey().getQualifiedName().toString());
            for ( ExecutableElement method: entry.getValue()) {
                importsNeeded.add(packageElement.getQualifiedName().toString() + '.' + ProducerWrapperWriter.buildWrapperName(entry.getKey(), method));
                producerWrapperWriter.write(method);
            }
        }

        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(BusProcessor.GEN_PACKAGE +'.'+ REGISTRAR_NAME);

        JavaWriter jw = new JavaWriter(jfo.openWriter());

        jw.emitPackage(BusProcessor.GEN_PACKAGE)
                .emitImports(importsNeeded.toArray(new String[importsNeeded.size()]))
                .beginType(REGISTRAR_NAME, "class", EnumSet.of(Modifier.PUBLIC), AbstractRegistrar.class.getSimpleName());

        writeSubcriptionMethod(subscriptionClassReference, jw);

        writeProducerMethod(productionClassReference, jw);


        jw.endType()
                .close();

    }

    /**
     mostly to fix an imaginary issue where some one annotates both the super class method and the overridden sub class method
     */
    private void removeOverloadedAnnotatedMethods(Map<TypeElement, Set<ExecutableElement>> methodToClassLookup) {
        Types typeUtils = processingEnv.getTypeUtils();
        Elements elementUtils = processingEnv.getElementUtils();
        Set<TypeElement> types = new HashSet<TypeElement>(methodToClassLookup.keySet());
        for ( TypeElement type: types){

            for(Iterator<Entry<TypeElement, Set<ExecutableElement>>> entriesIterator = methodToClassLookup.entrySet().iterator(); entriesIterator.hasNext();){
                Entry<TypeElement, Set<ExecutableElement>> entry = entriesIterator.next();
                if ( typeUtils.isSameType(entry.getKey().asType(), type.asType())){
                    continue;
                }

                //true iff the first type is a subtype of the second
                if ( typeUtils.isSubtype(entry.getKey().asType(), type.asType())){
                    Set<ExecutableElement> methodsInSuperType = methodToClassLookup.get(type);
                    for( ExecutableElement superMethod: methodsInSuperType){
                        for ( Iterator<ExecutableElement> subMethodIterator = entry.getValue().iterator(); subMethodIterator.hasNext();) {
                            ExecutableElement subMethod = subMethodIterator.next();
                            //true iff the first method overrides the second
                            if ( elementUtils.overrides(subMethod, superMethod, type)){
                                subMethodIterator.remove();
                            }
                        }
                    }
                    if ( entry.getValue().isEmpty() ){
                        entriesIterator.remove();
                    }
                }
            }
        }
    }

    private void writeSubcriptionMethod(Map<TypeElement, Set<ExecutableElement>> classReference, JavaWriter jw) throws IOException {
        jw.emitAnnotation(Override.class)
                .beginMethod("Set<AbstractSubscriptionWrapper>", "getSubscriptionWrappers", EnumSet.of(Modifier.PUBLIC), "Object", "object")
                .emitStatement("Set<AbstractSubscriptionWrapper> wrappers = new HashSet<AbstractSubscriptionWrapper>()");

        for ( Entry<TypeElement, Set<ExecutableElement>> entry: classReference.entrySet() ){
            jw.beginControlFlow("if ( object instanceof %s)", entry.getKey().getSimpleName().toString());
            for( ExecutableElement subscription: entry.getValue() ){
                jw.emitStatement("wrappers.add(new %s())", SubscriptionWrapperWriter.buildWrapperName(entry.getKey(), subscription));
            }
            jw.endControlFlow();

        }

        jw.emitStatement("populateWrappers(object,wrappers)")
                .emitStatement("return wrappers")
                .endMethod();
    }

    private void writeProducerMethod(Map<TypeElement, Set<ExecutableElement>> classReference, JavaWriter jw) throws IOException {
        jw.emitAnnotation(Override.class)
                .beginMethod("Set<AbstractProducerWrapper>", "getProducersWrappers", EnumSet.of(Modifier.PUBLIC), "Object", "object")
                .emitStatement("Set<AbstractProducerWrapper> wrappers = new HashSet<AbstractProducerWrapper>()");

        for ( Entry<TypeElement, Set<ExecutableElement>> entry: classReference.entrySet() ){
            jw.beginControlFlow("if ( object instanceof %s)", entry.getKey().getSimpleName().toString());
            for( ExecutableElement subscription: entry.getValue() ){
                jw.emitStatement("wrappers.add(new %s())", ProducerWrapperWriter.buildWrapperName(entry.getKey(), subscription));
            }
            jw.endControlFlow();

        }

        jw.emitStatement("populateWrappers(object,wrappers)")
                .emitStatement("return wrappers")
                .endMethod();
    }
}
