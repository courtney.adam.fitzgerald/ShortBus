package net.riotopsys.shortbus.compiler;

/**
 * Created by adam.fitzgerald on 6/20/14.
 */
public class ShortbusCompiliationException extends Exception {

    public ShortbusCompiliationException(String s) {
        super(s);
    }

}
