package net.riotopsys.shortbus.compiler;

import com.squareup.javawriter.JavaWriter;

import net.riotopsys.shortbus.api.abstractions.AbstractSubscriptionWrapper;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

/**
 * Created by adam.fitzgerald on 6/21/14.
 */
public class SubscriptionWrapperWriter {

    private final static String FILE_NAME_FORMAT = "SubscriptionWrapper$$%s$$%s";

    private final ProcessingEnvironment processingEnv;

    public SubscriptionWrapperWriter(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    public void write(Set<ExecutableElement> subscribeMethods) throws IOException {
        for ( ExecutableElement element: subscribeMethods ){
            write(element);
        }
    }

    public void write(ExecutableElement element) throws IOException {

        TypeElement classElement = Util.getTypeElement(element);
        PackageElement packageElement = Util.getPackageElement(element);
        TypeElement parameterElement = Util.getParameterElement(element, processingEnv);

        String wrapperClassName = buildWrapperName(classElement, element);

        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(packageElement.getQualifiedName().toString() + '.' + wrapperClassName);

        new JavaWriter(jfo.openWriter())
                .emitPackage(packageElement.getQualifiedName().toString())
                .emitImports(AbstractSubscriptionWrapper.class.getCanonicalName(), parameterElement.getQualifiedName().toString())
                .beginType(wrapperClassName, "class", EnumSet.of(Modifier.PUBLIC), AbstractSubscriptionWrapper.class.getCanonicalName())

                .emitField(classElement.getQualifiedName().toString(), "instance", EnumSet.of(Modifier.PRIVATE))

                .emitAnnotation(Override.class)
                .beginMethod("Class", "getClassSubscribedFor", EnumSet.of(Modifier.PUBLIC))
                .emitStatement("return %s.class", parameterElement.getSimpleName())
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("void", "setWrappedInstance", EnumSet.of(Modifier.PUBLIC), "Object", "object")
                .emitStatement("instance = (%s)object", classElement.getSimpleName().toString())
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("Object","getWrappedInstance",EnumSet.of(Modifier.PUBLIC) )
                .emitStatement("return instance")
                .endMethod()

                .emitAnnotation(Override.class)
                .beginMethod("void","processEvent",EnumSet.of(Modifier.PUBLIC), "Object", "object" )
                .beginControlFlow("if ( active.get() )")
                .emitStatement("instance.%s((%s)object)", element.getSimpleName(), parameterElement.getSimpleName())
                .endControlFlow()
                .endMethod()

                .endType()
                .close();

    }

    public static String buildWrapperName(TypeElement classElement, ExecutableElement element) {
        return String.format(FILE_NAME_FORMAT, classElement.getSimpleName(), element.getSimpleName());
    }

}
