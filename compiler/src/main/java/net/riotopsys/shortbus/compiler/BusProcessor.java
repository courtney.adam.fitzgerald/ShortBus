package net.riotopsys.shortbus.compiler;

import net.riotopsys.shortbus.api.annotations.Produce;
import net.riotopsys.shortbus.api.annotations.Subscribe;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;

/**
 * Created by adam.fitzgerald on 6/20/14.
 */
@SupportedAnnotationTypes({
        "net.riotopsys.shortbus.api.annotations.Produce",
        "net.riotopsys.shortbus.api.annotations.Subscribe",
})
public class BusProcessor extends AbstractProcessor {

    public static final String GEN_PACKAGE = "net.riotopsys.shortbus.gen";

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    private Set<ExecutableElement> subscribeMethods = new HashSet<ExecutableElement>();
    private Set<ExecutableElement> produceMethods = new HashSet<ExecutableElement>();

    private boolean lock = false;

    @Override
    public boolean process(Set<? extends TypeElement> typeElements, RoundEnvironment roundEnvironment) {
        if ( lock ){
            return true;
        }
        lock = true;


        Messager messager = processingEnv.getMessager();
        for (TypeElement te : typeElements) {
            for (Element e : roundEnvironment.getElementsAnnotatedWith(te)) {
                ExecutableElement element = (ExecutableElement) e;

                try {
                    if (te.toString().equals(Subscribe.class.getCanonicalName())) {
                        handleSubscription(element);
                    } else if (te.toString().equals(Produce.class.getCanonicalName())) {
                        handleProduce(element);
                    }
                } catch (ShortbusCompiliationException exception ){
                    messager.printMessage(Diagnostic.Kind.ERROR, exception.getMessage());
                }
            }
        }

        try {
            new RegistrarWriter(processingEnv).write(subscribeMethods, produceMethods);

            new BusWriter( processingEnv ).write();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    private void handleSubscription(ExecutableElement element) throws ShortbusCompiliationException {
        Element parentElement = element.getEnclosingElement();

        if ( !Util.isClassOrInterface(parentElement) ){
            throw new ShortbusCompiliationException(String.format("Subscribe annotation cannot be placed on '%s'", parentElement.toString() ));
        }

        if ( !Util.isPublicEnough(element) ){
            throw new ShortbusCompiliationException(String.format("Subscribe annotation cannot be placed on private method '%s.%s'", parentElement.toString(), element.toString() ));
        }

        if ( !Util.hasVoidReturn( element ) ){
            throw new ShortbusCompiliationException(String.format("Subscribe annotation cannot be placed on non void method '%s.%s'", parentElement.toString(), element.toString() ));
        }

        if ( element.getParameters().size() > 1){
            throw new ShortbusCompiliationException(String.format("Subscribe annotation cannot be placed on method '%s.%s' as it has too many parameters", parentElement.toString(), element.toString() ));
        }

        if ( element.getParameters().isEmpty() ){
            throw new ShortbusCompiliationException(String.format("Subscribe annotation cannot be placed on method '%s.%s' as it has no parameters", parentElement.toString(), element.toString() ));
        }

        subscribeMethods.add(element);

    }

    private void handleProduce(ExecutableElement element) throws ShortbusCompiliationException {
        Element parentElement = element.getEnclosingElement();

        if ( !Util.isClassOrInterface(parentElement) ){
            throw new ShortbusCompiliationException(String.format("Produce annotation cannot be placed on '%s'", parentElement.toString() ));
        }

        if ( !Util.isPublicEnough(element) ){
            throw new ShortbusCompiliationException(String.format("Produce annotation cannot be placed on private method '%s.%s'", parentElement.toString(), element.toString() ));
        }

        if ( Util.hasVoidReturn( element ) ){
            throw new ShortbusCompiliationException(String.format("Produce annotation cannot be placed on void method '%s.%s'", parentElement.toString(), element.toString() ));
        }

        if ( !element.getParameters().isEmpty() ){
            throw new ShortbusCompiliationException(String.format("Produce annotation cannot be placed on method '%s.%s' as it has parameters", parentElement.toString(), element.toString() ));
        }

        produceMethods.add(element);
    }

}
