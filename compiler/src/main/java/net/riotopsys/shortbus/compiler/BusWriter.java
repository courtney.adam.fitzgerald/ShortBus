package net.riotopsys.shortbus.compiler;

import com.squareup.javawriter.JavaWriter;

import net.riotopsys.shortbus.api.abstractions.AbstractBus;
import net.riotopsys.shortbus.api.abstractions.AbstractRegistrar;

import java.io.IOException;
import java.util.EnumSet;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.tools.JavaFileObject;

/**
 * Created by adam.fitzgerald on 6/22/14.
 */
public class BusWriter {
    private final ProcessingEnvironment processingEnv;

    public BusWriter(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    public void write() throws IOException {
        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(BusProcessor.GEN_PACKAGE + ".Bus");

        new JavaWriter(jfo.openWriter()).emitPackage(BusProcessor.GEN_PACKAGE)
                .emitImports(AbstractBus.class, AbstractRegistrar.class)
                .beginType("Bus","class", EnumSet.of(Modifier.PUBLIC), AbstractBus.class.getSimpleName())
                .emitField(AbstractRegistrar.class.getSimpleName(),"registrar",EnumSet.of(Modifier.PRIVATE,Modifier.FINAL),"new Registrar()")
                .emitAnnotation(Override.class)
                .beginMethod(AbstractRegistrar.class.getSimpleName(), "getRegistrar", EnumSet.of(Modifier.PROTECTED))
                .emitStatement("return registrar")
                .endMethod()
                .endType()
                .close();
    }
}
