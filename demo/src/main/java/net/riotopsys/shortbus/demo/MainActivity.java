package net.riotopsys.shortbus.demo;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import net.riotopsys.shortbus.api.annotations.Subscribe;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 0xDEADFEED;
    public static final float MAX_ZOOM = 18f;
    private GoogleMap map;
    private Circle circle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProducer.getBus().register(this);
        if (checkPlayServices()) {
            startService(new Intent(this,LocationService.class));
        }
        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported.",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Google Play Services must be installed.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProducer.getBus().unregister(this);
    }

    @Subscribe
    protected void onLocation( android.os.Parcelable bob ){
        Location location = (Location)bob;
        LatLng latlng = locationToLatLng(location);

        if ( map.getCameraPosition().zoom > MAX_ZOOM) {
            map.animateCamera(CameraUpdateFactory.newLatLng(latlng));
        } else {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, MAX_ZOOM));
        }

        if ( circle == null ) {
            CircleOptions circleOptions = new CircleOptions()
                    .fillColor(0x554169E1)
                    .strokeColor(0xAA4169E1)
                    .strokeWidth(6)
                    .center(latlng)
                    .radius(location.getAccuracy()); // In meters
            circle = map.addCircle(circleOptions);
        } else {
            circle.setCenter(latlng);
            circle.setRadius(location.getAccuracy());
        }

    }

    private LatLng locationToLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

}