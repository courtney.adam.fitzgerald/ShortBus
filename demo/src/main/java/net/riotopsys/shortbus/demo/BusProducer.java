package net.riotopsys.shortbus.demo;

import net.riotopsys.shortbus.api.IBus;
import net.riotopsys.shortbus.gen.Bus;

/**
 * Created by adam.fitzgerald on 6/22/14.
 */
public class BusProducer {

    public static final IBus bus;

    static{
        bus = new Bus();
    }

    public static IBus getBus(){
        return bus;
    }


}
