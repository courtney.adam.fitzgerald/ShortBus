package net.riotopsys.shortbus.demo;

import android.location.Location;

import net.riotopsys.shortbus.api.annotations.Produce;
import net.riotopsys.shortbus.api.annotations.Subscribe;

/**
 * Created by adam.fitzgerald on 7/9/14.
 */
public interface IProducer {

    @Produce
    public Location provideLastLocation();

    @Produce
    public LocationList provideLocationList();


    public void onLocation( Location location );

}
