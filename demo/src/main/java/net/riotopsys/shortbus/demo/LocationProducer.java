package net.riotopsys.shortbus.demo;

import android.location.Location;

import net.riotopsys.shortbus.api.annotations.Produce;
import net.riotopsys.shortbus.api.annotations.Subscribe;

/**
 * Created by adam.fitzgerald on 6/27/14.
 */
public class LocationProducer implements IProducer{


    private Location location;
    private LocationList locationList = new LocationList();

    @Produce
    public Location provideLastLocation(){
        return location;
    }

    @Produce
    public LocationList provideLocationList(){
        return locationList;
    }

    @Subscribe
    public void onLocation( Location location ){
        this.location = location;
        locationList.addLast(location);
    }
}
